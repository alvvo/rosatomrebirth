package com.example.rosatomrebirth;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import androidx.appcompat.app.AppCompatActivity;

public class BottomNavigationActivity extends AppCompatActivity {

    TextView textView;
    DBHelper dbHelper = new DBHelper();

    LinearLayout newsPage;
    Button newsButtonPage;

    LinearLayout chatPage;
    Button chatButtonPage;

    LinearLayout meetupPage;
    Button meetupButtonPage;

    LinearLayout rosatomPage;
    Button rosatomButtonPage;

    LinearLayout profilePage;
    Button profileButtonPage;

    //Idea
    EditText ideaHeader;
    EditText ideaMessage;

    //lottie

    LottieAnimationView profileLottie;

    //profile data

    TextView user_profile_name;
    TextView user_profile_uid;
    TextView user_profile_email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottom_navigation);
        init();
        newsButtonPage.performClick();
        /*textView=findViewById(R.id.read);
        textView.setText("Загрузка...");

        Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    readStringFromDatabase("USER_DATA",dbHelper.mAuth.getUid(),"Name");
                    System.out.println(dbHelper.mAuth.getCurrentUser().getUid());
                }
            }, 1000);*/

    }

    public void init(){
        newsPage = (LinearLayout) findViewById(R.id.news_page_layout);
        chatPage = (LinearLayout) findViewById(R.id.chat_page_layout);
        meetupPage  = (LinearLayout) findViewById(R.id.meetup_page_layout) ;
        rosatomPage = (LinearLayout) findViewById(R.id.rosatom_page_layout);
        profilePage = (LinearLayout) findViewById(R.id.profile_page_layout);

        newsButtonPage = findViewById(R.id.news_button);
        chatButtonPage = findViewById(R.id.chat_button);
        meetupButtonPage = findViewById(R.id.meet_up);
        rosatomButtonPage = findViewById(R.id.atom);
        profileButtonPage = findViewById(R.id.profile);

        newsPage.setVisibility(View.INVISIBLE);
        chatPage.setVisibility(View.INVISIBLE);
        meetupPage.setVisibility(View.INVISIBLE);
        rosatomPage.setVisibility(View.INVISIBLE);
        profilePage.setVisibility(View.INVISIBLE);

        //IdeaSend

        ideaHeader = findViewById(R.id.idea_header);
        ideaMessage = findViewById(R.id.idea_comment);

        //lottie

        profileLottie = findViewById(R.id.lottie_user);

        //profile data

        user_profile_name = findViewById(R.id.user_profile_name);
        user_profile_uid = findViewById(R.id.user_profile_uid);
        user_profile_email = findViewById(R.id.user_profile_email);
    }

    public void readStringFromDatabase(String child, String child1, String child2){
        dbHelper.getDatabaseReference().addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.child(child).child(child1).child(child2).getValue(String.class);
                System.out.println(value);
                System.out.println(value);
                System.out.println(value);
                textView.setText(value);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
            }
        });
    }

    public void onNewsPageClick(View view) {
        resetAllMenuButtonColor();
        resetAllPages();
        newsPage.setVisibility(View.VISIBLE);
        Drawable image = this.getResources().getDrawable(R.drawable.ic_newspaper_pressed);
        image.setBounds( 0, 0, image.getIntrinsicWidth(), image.getIntrinsicHeight() );
        newsButtonPage.setCompoundDrawables(null,image,null,null);
        newsButtonPage.setTextColor(this.getResources().getColor(R.color.blue_mid));
    }

    public void onChatPageClick(View view) {
        resetAllMenuButtonColor();
        resetAllPages();
        chatPage.setVisibility(View.VISIBLE);
        Drawable image = this.getResources().getDrawable(R.drawable.ic_chat_pressed);
        image.setBounds( 0, 0, image.getIntrinsicWidth(), image.getIntrinsicHeight() );
        chatButtonPage.setCompoundDrawables(null,image,null,null);
        chatButtonPage.setTextColor(this.getResources().getColor(R.color.blue_mid));
    }

    public void resetAllMenuButtonColor(){
        Drawable news = this.getResources().getDrawable(R.drawable.ic_newspaper);
        Drawable chat = this.getResources().getDrawable(R.drawable.ic_chat);
        Drawable meetUp = this.getResources().getDrawable(R.drawable.ic_meetup_gray);
        Drawable atom = this.getResources().getDrawable(R.drawable.ic_atom);
        Drawable profile = this.getResources().getDrawable(R.drawable.ic_user);

        news.setBounds( 0, 0, news.getIntrinsicWidth(), news.getIntrinsicHeight() );
        chat.setBounds( 0, 0, chat.getIntrinsicWidth(), chat.getIntrinsicHeight() );
        meetUp.setBounds( 0, 0, meetUp.getIntrinsicWidth(), meetUp.getIntrinsicHeight() );
        atom.setBounds( 0, 0, atom.getIntrinsicWidth(), atom.getIntrinsicHeight() );
        profile.setBounds( 0, 0, atom.getIntrinsicWidth(), atom.getIntrinsicHeight() );

        newsButtonPage.setCompoundDrawables(null,news,null,null);
        chatButtonPage.setCompoundDrawables(null,chat,null,null);
        meetupButtonPage.setCompoundDrawables(null,meetUp,null,null);
        rosatomButtonPage.setCompoundDrawables(null,atom,null,null);
        profileButtonPage.setCompoundDrawables(null,profile,null,null);

        newsButtonPage.setTextColor(this.getResources().getColor(R.color.dark_gray));
        chatButtonPage.setTextColor(this.getResources().getColor(R.color.dark_gray));
        meetupButtonPage.setTextColor(this.getResources().getColor(R.color.dark_gray));
        rosatomButtonPage.setTextColor(this.getResources().getColor(R.color.dark_gray));
        profileButtonPage.setTextColor(this.getResources().getColor(R.color.dark_gray));
    }

    public void resetAllPages(){
        newsPage.setVisibility(View.INVISIBLE);
        chatPage.setVisibility(View.INVISIBLE);
        meetupPage.setVisibility(View.INVISIBLE);
        rosatomPage.setVisibility(View.INVISIBLE);
        profilePage.setVisibility(View.INVISIBLE);
    }

    public void onCard0Click(View view) {
        Intent intent = new Intent(this, NewsWebViewActivity.class);
        intent.putExtra("url","https://www.rosatom.ru/journalist/news/posol-rf-v-bolivii-posetil-ploshchadku-sooruzheniya-tsentra-yadernykh-issledovaniy-i-tekhnologiy-tsya/");
        startActivity(intent);
    }

    public void onCard1Click(View view) {
        Intent intent = new Intent(this, NewsWebViewActivity.class);
        intent.putExtra("url","https://www.rosatom.ru/journalist/news/proshlo-zasedanie-obshchestvennogo-soveta-goskorporatsii-rosatom/");
        startActivity(intent);
    }

    public void onCard2Click(View view) {
        Intent intent = new Intent(this, NewsWebViewActivity.class);
        intent.putExtra("url","https://www.rosatom.ru/journalist/news/proekty-rosatoma-byli-otmecheny-v-ramkakh-konkursa-prof-it-innovatsiya/");
        startActivity(intent);
    }

    public void onMeetUpClick(View view) {
        resetAllMenuButtonColor();
        resetAllPages();
        meetupPage.setVisibility(View.VISIBLE);
        Drawable image = this.getResources().getDrawable(R.drawable.ic_meetup);
        image.setBounds( 0, 0, image.getIntrinsicWidth(), image.getIntrinsicHeight() );
        meetupButtonPage.setCompoundDrawables(null,image,null,null);
        meetupButtonPage.setTextColor(this.getResources().getColor(R.color.blue_mid));
    }

    public void onRosatomClick(View view) {
        resetAllMenuButtonColor();
        resetAllPages();
        rosatomPage.setVisibility(View.VISIBLE);
        Drawable image = this.getResources().getDrawable(R.drawable.ic_atom_pressed);
        image.setBounds( 0, 0, image.getIntrinsicWidth(), image.getIntrinsicHeight() );
        rosatomButtonPage.setCompoundDrawables(null,image,null,null);
        rosatomButtonPage.setTextColor(this.getResources().getColor(R.color.blue_mid));
    }

    public void onClickSendIdea(View view) {
        IdeaSender ideaSender = new IdeaSender(ideaHeader.getText().toString(),ideaMessage.getText().toString());
        ideaSender.sendIdea(this);
    }

    public void onProfileClick(View view) {

        Profile profile = new Profile();

        profile.setTextViewName(user_profile_name, "Name");
        user_profile_uid.setText("ID: "+dbHelper.mAuth.getUid());
        profile.setTextViewEmail(user_profile_email, "Email");

        resetAllMenuButtonColor();
        resetAllPages();
        profileLottie.playAnimation();
        profileLottie.setSpeed(1);
        profilePage.setVisibility(View.VISIBLE);
        Drawable image = this.getResources().getDrawable(R.drawable.ic_user_pressed);
        image.setBounds( 0, 0, image.getIntrinsicWidth(), image.getIntrinsicHeight() );
        profileButtonPage.setCompoundDrawables(null,image,null,null);
        profileButtonPage.setTextColor(this.getResources().getColor(R.color.blue_mid));
    }
}
