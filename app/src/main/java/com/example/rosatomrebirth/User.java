package com.example.rosatomrebirth;

public class User {

    public String email;
    public String password;
    public String userName;
    public String userSurname;
    public String userSecondName;
    public String employeeID;
    public String birthday_day;
    public String birthday_month;
    public String birthday_year;

    public User(){
        email="";
        password="";
        userName="";
        userSurname="";
        userSecondName="";
        employeeID="";
        birthday_day="";
        birthday_month="";
        birthday_year="";
    }

    public User(
            String userName,
            String userSurname,
            String userSecondName,
            String birthday_day,
            String birthday_month,
            String birthday_year,
            String email,
            String password,
            String employeeID
    ){
        this.email=email;
        this.password=password;
        this.userName=userName;
        this.userSurname=userSurname;
        this.userSecondName=userSecondName;
        this.employeeID=employeeID;
        this.birthday_day=birthday_day;
        this.birthday_month=birthday_month;
        this.birthday_year=birthday_year;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserSurname() {
        return userSurname;
    }

    public void setUserSurname(String userSurname) {
        this.userSurname = userSurname;
    }

    public String getUserSecondName() {
        return userSecondName;
    }

    public void setUserSecondName(String userSecondName) {
        this.userSecondName = userSecondName;
    }

    public String getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(String employeeID) {
        this.employeeID = employeeID;
    }

    public String getBirthday_day() {
        return birthday_day;
    }

    public void setBirthday_day(String birthday_day) {
        this.birthday_day = birthday_day;
    }

    public String getBirthday_month() {
        return birthday_month;
    }

    public void setBirthday_month(String birthday_month) {
        this.birthday_month = birthday_month;
    }

    public String getBirthday_year() {
        return birthday_year;
    }

    public void setBirthday_year(String birthday_year) {
        this.birthday_year = birthday_year;
    }

}
