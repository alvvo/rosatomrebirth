package com.example.rosatomrebirth;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class LoadingActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    User newUser;
    String UID="Jon Doe";
    boolean validate;

    private String email;
    private String password;
    private String method;
    private String userName;
    private String userSurname;
    private String userSecondName;
    private String passwordRepeat;
    private String employeeID;
    private String birthday_day;
    private String birthday_month;
    private String birthday_year;

    DBHelper dbHelper = new DBHelper();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);
        init();

        action(method);

    }

    public void createNewUserInfo(){
        if(password.equals(passwordRepeat)){
            validate = true;
            signing(email,password,validate);
        }
        else{
            Toast.makeText(this, "Пароли не совпадают", Toast.LENGTH_SHORT).show();
            goBack(method);
        }
    }

    private void action(String method){
        if(method.equals("login")){
            signIn();
        }
        else if(method.equals("registration")){
            signUp();
        }
    }

    private void goBack(String method){
        if(method.equals("login")){
            Intent intent = new Intent(this,LoginActivity.class);
            startActivity(intent);
        }
        else if(method.equals("registration")){
            Intent intent = new Intent(this,RegistrationActivity.class);
            startActivity(intent);
        }
    }

    private void signIn(){
        if(!(email.equals("")&&password.equals(""))){
            signing(email,password,validate);
        }
        else{
            goBack(method);
            Toast.makeText(this, "Заполните поля ввода", Toast.LENGTH_SHORT).show();
        }
    }

    private void signUp(){
        if(!(email.equals("")&&password.equals(""))){
            registration(email,password);
        }
        else{
            goBack(method);
            Toast.makeText(this, "Заполните поля ввода", Toast.LENGTH_SHORT).show();
        }
    }

    public void sendIntentToMainMenu(){
        Intent intent = new Intent(this,BottomNavigationActivity.class);
        startActivity(intent);
    }

    private void signing(@NonNull String email, @NonNull String password, boolean validate){
        mAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    Toast.makeText(LoadingActivity.this, "Авторизация успешна!", Toast.LENGTH_SHORT).show();
                    if(validate){
                        UID = mAuth.getUid();
                        dbHelper.writeToDataBase("USER_DATA",UID,"Email",email);
                        dbHelper.writeToDataBase("USER_DATA",UID,"Name",userName);
                        dbHelper.writeToDataBase("USER_DATA",UID,"Surname",userSurname);
                        dbHelper.writeToDataBase("USER_DATA",UID,"SecondName",userSecondName);
                        dbHelper.writeToDataBase("USER_DATA",UID,"Birthday_day",birthday_day);
                        dbHelper.writeToDataBase("USER_DATA",UID,"Birthday_month",birthday_month);
                        dbHelper.writeToDataBase("USER_DATA",UID,"Birthday_year",birthday_year);
                        dbHelper.writeToDataBase("USER_DATA",UID,"E-ID",employeeID);
                    }
                    sendIntentToMainMenu();
                }
                else{
                    goBack(method);
                    Toast.makeText(LoadingActivity.this, "Ошибка авторизации", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void registration(String email, String password){
        mAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    Toast.makeText(LoadingActivity.this, "Регистрация успешна!", Toast.LENGTH_SHORT).show();
                    createNewUserInfo();
                }
                else{
                    goBack(method);
                    Toast.makeText(LoadingActivity.this, "Ошибка регистрации", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void init(){
        mAuth = FirebaseAuth.getInstance();

        Intent intent = getIntent();

        validate = false;

        email=intent.getStringExtra("email");
        password=intent.getStringExtra("password");
        method=intent.getStringExtra("method");
        if(method.equals("registration")){
            passwordRepeat=intent.getStringExtra("passwordRepeat");
            userName=intent.getStringExtra("userName");
            userSurname=intent.getStringExtra("userSurname");
            userSecondName=intent.getStringExtra("userSecondName");
            userName=intent.getStringExtra("userName");
            birthday_day=intent.getStringExtra("birthday_day");
            birthday_month=intent.getStringExtra("birthday_month");
            birthday_year=intent.getStringExtra("birthday_year");
            employeeID=intent.getStringExtra("employeeID");
        }
    }
}