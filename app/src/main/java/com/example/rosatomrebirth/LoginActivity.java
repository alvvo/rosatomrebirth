package com.example.rosatomrebirth;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginActivity extends AppCompatActivity {

    private EditText email;
    private EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        email = findViewById(R.id.editTextTextEmailAddress);
        password = findViewById(R.id.editTextTextEmailAddress2);

    }

    public void onClickSignIn(View view) {
        sendIntentToLoadingScreen();
    }

    public void sendIntentToLoadingScreen(){
        Intent intent = new Intent(this,LoadingActivity.class);
        intent.putExtra("email",email.getText().toString());
        intent.putExtra("password",password.getText().toString());
        intent.putExtra("method","login");
        startActivity(intent);
    }

    public void onClickSignUp(View view) {
        Intent intent = new Intent(this,RegistrationActivity.class);
        startActivity(intent);
    }
}