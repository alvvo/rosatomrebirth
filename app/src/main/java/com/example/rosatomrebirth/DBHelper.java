package com.example.rosatomrebirth;

import android.app.Application;
import android.content.Context;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.util.JsonUtils;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class DBHelper {

    FirebaseAuth mAuth =FirebaseAuth.getInstance();

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference();

    public DatabaseReference getDatabaseReference(){
        return myRef;
    }

    public void writeToDataBase(String child, String value){
        myRef.child(child).setValue(value).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
               // Toast.makeText(LoadingActivity.this, "Data is Saved", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void writeToDataBase(String child, String child1, String value){
        myRef.child(child).child(child1).setValue(value).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                //Toast.makeText(LoadingActivity.this, "Data is Saved", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void writeToDataBase(String child, String child1, String child2, String value){
        myRef.child(child).child(child1).child(child2).setValue(value).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                //Toast.makeText(LoadingActivity.this, "Data is Saved", Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void writeToDataBase(Context context, String child, String child1, String child2, String child3, String value){
        myRef.child(child).child(child1).child(child2).child(child3).setValue(value).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(context, "Data is Saved", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void readStringFromDatabase(TextView textView,String child, String child1, String child2){
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.child(child).child(child1).child(child2).getValue(String.class);
                textView.setText(value);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
            }
        });
    }

    public void readNameFromDatabase(TextView textView,String child, String child1){
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.child(child).child(child1).child("Name").getValue(String.class);
                String value1 = dataSnapshot.child(child).child(child1).child("Surname").getValue(String.class);
                String value2 = dataSnapshot.child(child).child(child1).child("SecondName").getValue(String.class);

                String result = value + " " + value1 + " " + value2;

                textView.setText(result);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
            }
        });
    }

    /*public String readStringFromDatabase(String child, String child1){
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                list.clear();
                String value = dataSnapshot.child(child).child(child1).getValue(String.class);
                list.add(value);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
            }
        });
        return list.get(1);
    }

    public void readStringFromDatabase(String child, String child1, String child2){
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                list.clear();
                String value = dataSnapshot.child(child).child(child1).child(child2).getValue(String.class);
                list.add(value);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
            }
        });
    }*/

    public FirebaseAuth getmAuth(){
        return mAuth;
    }

}
