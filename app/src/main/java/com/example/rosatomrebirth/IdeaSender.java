package com.example.rosatomrebirth;

import android.content.Context;

public class IdeaSender {

    private String HEADER;
    private String MESSAGE;

    DBHelper dbHelper = new DBHelper();

    public IdeaSender(){
        HEADER="";
        MESSAGE="";
    }

    public IdeaSender(String HEADER, String MESSAGE){
        this.HEADER=HEADER;
        this.MESSAGE=MESSAGE;
    }

    public void sendIdea(Context context){
        String UID = dbHelper.mAuth.getUid();
        dbHelper.writeToDataBase(context,"USERS_IDEAS",UID,HEADER,"Message",MESSAGE);
    }

}
