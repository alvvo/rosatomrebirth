package com.example.rosatomrebirth;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class RegistrationActivity extends AppCompatActivity {

    private EditText userName;
    private EditText userSurname;
    private EditText userSecondName;
    private EditText email;
    private EditText password;
    private EditText passwordRepeat;
    private EditText employeeID;
    private Spinner birthday_day;
    private Spinner birthday_month;
    private Spinner birthday_year;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        userName = findViewById(R.id.user_name);
        userSurname = findViewById(R.id.user_surname);
        userSecondName = findViewById(R.id.user_second_name);
        email = findViewById(R.id.user_email);
        password = findViewById(R.id.init_password);
        passwordRepeat = findViewById(R.id.repeat_password);
        employeeID = findViewById(R.id.user_id_workpass_number);
        birthday_day = findViewById(R.id.birthday_day);
        birthday_month = findViewById(R.id.birthday_mounth);
        birthday_year = findViewById(R.id.birthday_year);

    }

    public void onClickCreateProfile(View view) {
        sendIntentToLoadingScreen();
    }

    public void sendIntentToLoadingScreen(){
        Intent intent = new Intent(this,LoadingActivity.class);
        intent.putExtra("userName",userName.getText().toString());
        intent.putExtra("userSurname",userSurname.getText().toString());
        intent.putExtra("userSecondName",userSecondName.getText().toString());
        intent.putExtra("birthday_day",birthday_day.getSelectedItem().toString());
        intent.putExtra("birthday_month",birthday_month.getSelectedItem().toString());
        intent.putExtra("birthday_year",birthday_year.getSelectedItem().toString());
        intent.putExtra("email",email.getText().toString());
        intent.putExtra("password",password.getText().toString());
        intent.putExtra("passwordRepeat",passwordRepeat.getText().toString());
        intent.putExtra("employeeID",employeeID.getText().toString());
        intent.putExtra("method","registration");
        startActivity(intent);
    }

}