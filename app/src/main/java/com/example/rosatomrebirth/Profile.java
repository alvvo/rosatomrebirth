package com.example.rosatomrebirth;

import android.widget.TextView;

public class Profile {

    private String NAME;
    private String UID;
    private String EMAIL;

    DBHelper dbHelper = new DBHelper();

    public Profile(){
        NAME = "";
        UID = "";
        EMAIL = "";
    }

    public Profile(String NAME, String UID, String EMAIL){
        this.NAME = NAME;
        this.UID = UID;
        this.EMAIL = EMAIL;
    }

    public void setTextViewName(TextView textView, String parameter){
        dbHelper.readNameFromDatabase(textView,"USER_DATA",dbHelper.mAuth.getUid());
    }

    public void setTextViewEmail(TextView textView, String parameter){
        dbHelper.readStringFromDatabase(textView,"USER_DATA",dbHelper.mAuth.getUid(),parameter);
    }

}
